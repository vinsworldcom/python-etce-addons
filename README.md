# Python ETCE Wrappers additions

Wrappers for the Python ETCE framework for VLC and other miscellaneous add-ons.


## Files

Module                    | Description
----                      | ----
utils/brctl.py            | Run the `brctl` commands provided in input file.
utils/daemonizeCommand.py | Run the command provided in the configuration file in the background.
utils/gpsd.py             | Run the `GPSd` service (fix bug in v1.2.10 of [Python-ETCE](https://github.com/adjacentlink/python-etce)).
utils/mgen.py             | Run the `mgen` traffic generator (fix bug in v1.2.10 of [Python-ETCE](https://github.com/adjacentlink/python-etce)).
utils/mosquitto.py        | Run the `mosquitto` MQTT broker.
utils/redis.py            | Run the `redis` database server.
utils/runCommand.py       | Run the command provided in the configuration file.
utils/tcpdump.py          | Run the `tcpdump` command with options provided in the configuration file.
vlc/send.py               | Send a video with VLC.
vlc/receive.py            | Receive a video with VLC.


## Dependencies

[Python-ETCE](https://github.com/adjacentlink/python-etce) v1.2.10 or greater.  Python3 support required.


## Installation

```
[sudo] python3 -mpip install [--prefix=/usr] .
```

---

Copyright (c) - 2024 The MITRE Corporation. All Rights Reserved.

Public release case number:  24-1190
